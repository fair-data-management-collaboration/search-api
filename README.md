# Search API

The API developed has been integrated with the two standard ICAT and
SciCat data catalogs of the PaN community and can be extended beyond
that.  It is complemented by a federated service demonstrator for
searching open PaN data.  The main feature of this service is that it
allows any other facility to join the federated search thus enabling
all scientists to find datasets and publications from any number of
configured sites, based on metadata specific to the scientific domain
concerned.  The federated search service is accompanied by a web
interface (https://data.panosc.eu/) already deployed and tested in
partner facilities.  The search API and the underlying data model is a
useful first prototype, but needs further improvement and development.
It is intended to become a tool to be maintained in future
collaboration.
